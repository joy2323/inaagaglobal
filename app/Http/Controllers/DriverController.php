<?php

namespace App\Http\Controllers;

use App\Driver;
use Illuminate\Http\Request;

class DriverController extends Controller
{
    

    public function index()
    {
        $drivers=Driver::all();
        return response()->json($drivers);
    }

    public function login(Request $request, Driver $driver)
    {
       
        $driver=Driver::whereEmail($request->email)->first();
        if(!$driver){
            return response()->json([
                'status'=>404,
                'message'=>'User not found'
            ]);
        // }else{
        //     if($driver->password!==$request->password){
        //         return response()->json([
        //             'status'=>405,
        //             'message'=>'Wrong authentication credentials'
        //         ]); 
            }elseif($driver->email==$request->email){
                return response()->json([
                    'status'=>201,
                    'message'=>'Successful login',
                    // 'result'=>$driver
                ]);
             }
         //}
    }

    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        if(count(Driver::whereEmail($request->email)->get())>=1){
            return response()->json([
                'status'=>501,
                'message'=>'Email address already exist'
            ]);
        }
        else if(count(Driver::wherePhone_no($request->phone_no)->get()) >=1){
            return response()->json([
                'status'=>503,
                'message'=>'Phone number already exist'
            ]);
        }else{
        $driver= new Driver([
            'name'=>$request->name,
            'email'=>$request->email,
            'phone_no'=>$request->phone_no,
            'password'=>\Hash::make($request->password)
            ]);
       if ($driver->save()){
        return response()->json([
            'status'=> 200,
            'message'=> 'Successful'
        ]);
        }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $driver=Driver::whereId($id)->first();
        $driver->password=\Hash::make($request->password);
            if($driver->save()){
            return response()->json([
                'status'=>201,
                'message'=> 'Password Updated...'
            ]);
             }else{
                return response()->json([
                        'status' =>419,
                        'message' => 'Password not updated'
                ]);
            }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Driver $driver)
    {
        
        if($driver->delete()){
            return response()->json([ 
                'status'=>200,
                'message'=>'Driver details was successfully deleted'
            ]);
            }else{
                    return response()->json([
                   'status'=> 500,
                   'message'=>'Driver details not deleted',
               ]);
            } 
    }
}
